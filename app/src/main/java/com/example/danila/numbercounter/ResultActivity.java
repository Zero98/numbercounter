package com.example.danila.numbercounter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.opencv.core.Mat;

import java.io.File;

public class ResultActivity extends AppCompatActivity {
    private TextView tvCountOne;
    private TextView tvCountZero;
    private ImageView imageView;
    private Bitmap grayBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Bundle arguments = getIntent().getExtras();
        grayBitmap = (Bitmap)arguments.get("grayBitmap");

        tvCountOne = findViewById(R.id.one_count);
        tvCountZero = findViewById(R.id.zero_count);
        imageView = findViewById(R.id.imageView);

        imageView.setImageBitmap(grayBitmap);
    }
}
